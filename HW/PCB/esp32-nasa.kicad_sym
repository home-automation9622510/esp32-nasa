(kicad_symbol_lib
	(version 20231120)
	(generator "kicad_symbol_editor")
	(generator_version "8.0")
	(symbol "TPSM84209RKHR"
		(exclude_from_sim no)
		(in_bom yes)
		(on_board yes)
		(property "Reference" "IC"
			(at 11.43 12.7 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
			)
		)
		(property "Value" "TPSM33615FRDNR"
			(at 11.43 10.16 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
			)
		)
		(property "Footprint" "TPSM84209RKHT"
			(at 26.67 -94.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
				(hide yes)
			)
		)
		(property "Datasheet" "https://www.ti.com/lit/ds/symlink/tpsm84209.pdf?ts=1683996447343&ref_url=https%253A%252F%252Fwww.ti.com%252Fproduct%252FTPSM84209"
			(at 26.67 -194.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
				(hide yes)
			)
		)
		(property "Description" "Switching Voltage Regulators 4.5-V to 28-V input, 1.2-V to 6-V output, 2.5-A power module 9-QFN-FCMOD -40 to 85"
			(at -2.032 14.732 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(hide yes)
			)
		)
		(property "Height" "2.1"
			(at 26.67 -394.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
				(hide yes)
			)
		)
		(property "Mouser Part Number" "595-TPSM84209RKHR"
			(at 26.67 -494.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
				(hide yes)
			)
		)
		(property "Mouser Price/Stock" "https://www.mouser.co.uk/ProductDetail/Texas-Instruments/TPSM84209RKHR?qs=T3oQrply3y%2FZUEJZF08Puw%3D%3D"
			(at 26.67 -594.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
				(hide yes)
			)
		)
		(property "Manufacturer_Name" "Texas Instruments"
			(at 26.67 -694.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
				(hide yes)
			)
		)
		(property "Manufacturer_Part_Number" "TPSM84209RKHR"
			(at 26.67 -794.92 0)
			(effects
				(font
					(size 1.27 1.27)
				)
				(justify left top)
				(hide yes)
			)
		)
		(symbol "TPSM84209RKHR_1_1"
			(rectangle
				(start -10.16 7.62)
				(end 10.16 -7.62)
				(stroke
					(width 0.254)
					(type default)
				)
				(fill
					(type background)
				)
			)
			(pin open_collector line
				(at 12.7 6.35 180)
				(length 2.54)
				(name "PGOOD"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at 0 -10.16 90)
				(length 2.54)
				(name "GND"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "10"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -12.7 -2.54 0)
				(length 2.54)
				(name "RT"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "11"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at -12.7 2.54 0)
				(length 2.54)
				(name "EN"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_in line
				(at -12.7 6.35 0)
				(length 2.54)
				(name "VIN"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "3"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_out line
				(at 12.7 2.54 180)
				(length 2.54)
				(name "VOUT"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "4"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin no_connect line
				(at 0 2.54 180)
				(length 0) hide
				(name "SW_1"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "5"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin no_connect line
				(at 0 3.81 180)
				(length 0) hide
				(name "SW_2"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "6"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin no_connect line
				(at 0 1.27 180)
				(length 0) hide
				(name "BOOT"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "7"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin power_out line
				(at -12.7 -6.35 0)
				(length 2.54)
				(name "VCC"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "8"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
			(pin input line
				(at 12.7 -2.54 180)
				(length 2.54)
				(name "FB"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
				(number "9"
					(effects
						(font
							(size 1.27 1.27)
						)
					)
				)
			)
		)
	)
)